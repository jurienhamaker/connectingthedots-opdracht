import './slider/slider';
import './swatch/swatch';
import './product-slider/product-slider';
import './dropdown/dropdown';
import './specs-and-download/specs-and-download';
import './inspiration-gallery/inspiration-gallery';