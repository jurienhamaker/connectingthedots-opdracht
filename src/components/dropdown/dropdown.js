const initializeDropdown = dropdown => {
    // set selected option text as selected text.

    selectDropdownIndex(dropdown, 0);

    dropdown.click((e) => toggleDropdown(e, dropdown));

    dropdown.find('.list p').click((e) => selectOption(e));
};

const selectDropdownIndex = (dropdown, index) => {
    const selected = dropdown.find('.list .selected').eq(0);
    const newSelected = dropdown.find('.list p').eq(index);
    
    selected.removeClass('selected');
    newSelected.addClass('selected');

    dropdown.find('.box p').first().text(newSelected.text()); 
};

const toggleDropdown = (e, dropdown, force = false) => {
    const target = $(e.target);

    if(target.parent().hasClass('box') || target.hasClass('box') || force) {
        dropdown.toggleClass('active');
    }
}

const selectOption = e => {
    const target = $(e.target);
    const dropdown = target.parent().parent();
    selectDropdownIndex(dropdown, target.index());
    toggleDropdown(e, dropdown, true)
}

$(document).ready(() => {
    $('.dropdown').each((index, item) => initializeDropdown($(item)));
});