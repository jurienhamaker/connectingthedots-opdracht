const toggleDetailsContent = header => {
    const item = header.parent();

    if(item.hasClass('active')) {
        item.removeClass('active');
        item.find('.header span').first().text('+');
        return;
    }

    const currentActive = item.parent().find('.active').first();
    currentActive.removeClass('active');
    currentActive.find('.header span').first().text('+');

    item.addClass('active');
    item.find('.header span').first().text('-');
}

$(document).ready(() => {
    $('.details-list .header').click(e => toggleDetailsContent($(e.target)));
});