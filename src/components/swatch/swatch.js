const selectSwatch = swatch => {
    swatch = $(swatch);

    const parent = swatch.parent();
    const activeSwatch = parent.find('.selected');

    if(activeSwatch) {
        activeSwatch.removeClass('selected');
    }

    swatch.addClass('selected');
};

$('document').ready(() => {
    // swatches on first slide selection
    $('.swatches span').click(e => selectSwatch(e.target));
});