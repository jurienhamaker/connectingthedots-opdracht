const selectTag = target => {
    if(target.hasClass('selected')) return; // only toggle off when clicking cross
    
    if(target.hasClass('remove')) {
        target = target.parent();
    }

    target.toggleClass('selected');
};

$(document).ready(() => {
    $('.inspiration-gallery .tags .item').click(e => selectTag($(e.target)));

    $('.inspiration-gallery .tags .item').each((index, target) => {
        const removeElement = document.createElement('img');
        removeElement.classList.add('remove');
        removeElement.src = "/assets/icons/x.svg";

        target.append(removeElement);
    })
});