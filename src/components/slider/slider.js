const slide = (sliderContainer, direction = 1) => {
    if(!sliderContainer) return;

    const newScrollPosition = 
        sliderContainer.scrollLeft() + 
            (
                sliderContainer.innerWidth() * 
                (direction === 1 ? 1 : -1)
            );

    sliderContainer[0].scrollTo({
        left: newScrollPosition,
        behavior: 'smooth'
    });
};

const slidersReset = () => {
    const sliders = $('.slider .slider-container');
    if(!sliders || !sliders.length) return;

    for(let i = 0; i < sliders.length; i++) {
        sliders[i].scrollTo({
            left: 0,
            behavior: 'smooth'
        })
    }
}

$('document').ready(() => {
    // get arrows and bind functionality
    $('.slider .arrow').click(function() {
        slide($(this).parent().find('.slider-container').first(), $(this).hasClass('next') ? 1 : -1)
    });
    // reset left offset when resizing
    $(window).resize(() => slidersReset());
});