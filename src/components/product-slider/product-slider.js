const productSliderSlide = (direction = 1) => {
    const sliderContainer = $('.product-slider .slider-container');
    if(!sliderContainer) return;

    const currentIndex = sliderContainer.find('.active').index();

    // new index is +1 or +-1;
    setActiveSlide(currentIndex + direction);
};

const setActiveSlide = (index) => {
    const slides = $('.product-slider .slider-container .item');
    const pills = $('.product-slider .controls .pills span');
    const currentActive = $('.product-slider .slider-container .item.active');
    const currentActivePill = $('.product-slider .controls .pills span.active');
    const nextActive = slides[index];
    const nextActivePill = pills[index];

    currentActive.removeClass('active');
    currentActivePill.removeClass('active');

    $(nextActive).addClass('active');
    $(nextActivePill).addClass('active');

    const sliderContainer = $('.product-slider .slider-container');
    if(!sliderContainer) return;

    // use native scrollTo functionality
    sliderContainer[0].scrollTo({
        left: nextActive.offsetLeft,
        behavior: 'smooth'
    });
};

const createPills = () => {
    const slides = $('.product-slider .slider-container .item');
    const pills = $('.product-slider .controls .pills');

    for(let i = 0; i < slides.length; i++) {
        pills.append(document.createElement('span'));
    }
};

$('document').ready(() => {
    // swatches on first slide selection
    $('.swatches span').click(e => selectSwatch(e.target));

    // initialize slider
    createPills();
    setActiveSlide(0);
    
    const arrows = $('.product-slider .controls .arrows .arrow');
    // arrows[0] = left
    $(arrows[0]).click(() => productSliderSlide(-1));
    // arrows[1] = left
    $(arrows[1]).click(() => productSliderSlide(1));

    // set active index by pill click
    $(".product-slider .controls .pills span").click(e => setActiveSlide($(e.target).index()));

    // disable mobile scrolling on the slider
    $(".product-slider .slider-container").bind('touchstart', e => {
        e.preventDefault();
        e.stopPropogation();
        e.handled = true;
    });

    $(window).resize(() => setActiveSlide(0));
});