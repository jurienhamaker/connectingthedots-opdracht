FROM node:12

# set workdir and exposed ports
WORKDIR /opt/app
EXPOSE 3000

# install firebase
RUN yarn global add firebase-tools

# copy modules files
COPY package.json yarn.lock /opt/app/

#install modules
RUN yarn

#copy app
COPY . /opt/app

# set entrypoint to yarn
ENTRYPOINT [ "yarn" ]